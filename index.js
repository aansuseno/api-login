const express = require('express')
const app = express()
const crypto = require('crypto')
const Model = require('./model')


// decode form
app.use(express.urlencoded())

// ambil jam terbaru
const jam = () => {
	const dt = new Date()
	return `${dt.getFullYear()}-${('0'+ (dt.getMonth()+1)).slice(-2)}-${('0'+ dt.getDate()).slice(-2)} ${('0'+ dt.getHours()).slice(-2)}:${('0'+ dt.getMinutes()).slice(-2)}:${('0'+ dt.getSeconds()).slice(-2)}`
}

app.post('/register', (req, res) => {

	// username dan password wajib
	if (!req.body.hasOwnProperty('username') && !req.body.hasOwnProperty('password')) {
		return res.status(500).json({error: 'Username dan Password wajib diisi.'})
	}

	var sekarang = jam()
	var dataInput = {
		username: req.body.username,
		password: req.body.password,
		name: (req.body.hasOwnProperty('name')) ? req.body.name : req.body.username,
		email: (req.body.hasOwnProperty('email')) ? req.body.email : '',
		note: (req.body.hasOwnProperty('note')) ? req.body.note : '',
		status: (req.body.hasOwnProperty('status')) ? req.body.status : '',
		created_at: sekarang,
		updated_at: sekarang
	}
	m = new Model('user')
	m.insert(dataInput)
	m.run((err, result) => {
		if (err) {
			return res.status(500).json({error: err.sqlMessage})
		} else {
			console.log(`${dataInput['username']} mendaftar`)
			return res.json({result: 'sekarang anda bisa login'})
		}
	})
})

app.post('/login', (req, res) => {
	if (!req.body.hasOwnProperty('username') && !req.body.hasOwnProperty('password')) {
		return res.status(500).json({error: 'Username dan Password wajib diisi.'})
	}

	m = new Model('user')
	m.select('id')
	m.where({
		username: req.body.username,
		password: req.body.password
	})
	m.limit(1)
	
	m.run((err, result) => {
		if (err) {
			return res.status(500).json({error: err.sqlMessage})
		} else if (result.length == 0 || !result[0].hasOwnProperty('id')) {
			return res.status(500).json({error: 'username & password tidak ditemukan'})
		}
		const id = result[0].id
		const sekarang = jam()

		var token = crypto.randomBytes(25).toString('hex')

		// cek apa user masih login
		mm = new Model('login')
		mm.select('*')
		mm.where({
			id_user: id
		})
		mm.limit(1)
		mm.run((err, result1) => {
			if (err) {
				return res.status(500).json({error: err.sqlMessage})
			}else if (result1.length > 0) {
				idYgLama = result1[0].id
				sudahLogin(idYgLama)
			} else {
				belumLogin()
			}
		})

		const sudahLogin = (idYgLama) => {
			m = new Model('login')
			m.update({
				token: token,
				updated_at: sekarang
			})
			m.where({
				id: idYgLama
			})
			m.run((err, result) => {
				if (err) {
					return res.status(500).json({error: err.sqlMessage})
				}
				return res.json({token: token})
			})
		}

		const belumLogin = () => {
			mmm = new Model('login')
			mmm.insert({
				id_user: id,
				token: token,
				created_at: sekarang,
				updated_at: sekarang
			})
			
			mmm.run((err, result1) => {
				if (err) {
					return res.status(500).json({error: err.sqlMessage})
				}
				return res.json({token: token})
			})
		}
	})
})

app.get('/logout/:username/:token', (req, res) => {
	cek(req.params.username, req.params.token, true, (hasil, error = '') => {
		if (!hasil) {
			return res.status(500).json({error: error})
		}
		userDitemukan(hasil)
	})

	const userDitemukan = (id) => {
		m = new Model('login')
		m.delete()
		m.where({
			id: id
		})
		m.run((err, result) => {
			if (err) {
				return res.status(500).json({error: err.sqlMessage})
			} else {
				return res.json({result: 'berhasil logout'})
			}
		})
	}
})

app.get('/cek/:username/:token', (req, res) => {
	cek(req.params.username, req.params.token, false, (hasil, error = '') => {
		if (hasil) {
			return res.json({result: true})
		} else {
			return res.json({result: false})
		}
	})
})

// untuk menecek user apakah aman atau tidak
const cek = (username, token, denganId, callback) => {

	m = new Model('login')
	m.select('*')
	m.where({
		token: token
	})
	m.limit(1)

	m.run((err, result) => {
		if (err) {
			callback(false, err.sqlMessage)
		} else if (result.length > 0) {
			tokenDitemukan(result[0])
		} else {
			callback(false, 'token tidak valid')
		}
	})

	const tokenDitemukan = (data) => {
		mm = new Model('user')
		mm.select('*')
		mm.where({
			id: data.id_user,
			username: username
		})
		mm.limit(1)
		mm.run((err, result) => {
			if (err) {
				callback(false, err.sqlMessage)
			} else if (result.length > 0) {
				if (denganId) {
					callback(data.id)
				} else {
					callback(true)
				}
			} else {
				callback(false, 'username tidak ditemukan')
			}
		})
	}
}

app.listen(3000, () => {
	console.log(`Jalan di localhost:3000`)
})