const mysql = require('mysql')
require('dotenv').config()

const con = mysql.createConnection({
	host: process.env.database_host,
	user: process.env.database_username,
	password: process.env.database_password,
	database: process.env.database_name,
})

con.connect((err) => {
	if (err) {console.error(err)}
	console.log(`Koneksi ke basisdata ${process.env.database_name} berhasil.`)
})

module.exports = con