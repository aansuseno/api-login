const db = require('./db')

class Model {
	constructor(new_table) {
		this.query = ``
		this.tbl = new_table
	}

	// untuk tambah data
	insert(data) {
		var fields = ``
		var values = ``

		for (const key in data) {
			fields+=`${key}, `
			values+=`${this.amankan(data[key])}, `
		}

		var new_fields = fields.substring(0, fields.length-2)
		var new_values = values.substring(0, values.length-2)

		this.query = `INSERT INTO ${this.tbl} (${new_fields}) values (${new_values})`
	}

	select(fields = '*') {
		this.query = `SELECT ${fields} FROM ${this.tbl}`
	}

	update(data) {
		var kondisi = ``

		for (const key in data) {
			kondisi += `${key} = ${this.amankan(data[key])}, `
		}

		var kondisi_new = kondisi.substring(0, kondisi.length-2)

		this.query = `UPDATE ${this.tbl} SET ${kondisi_new}`
	}

	delete() {
		this.query = `DELETE FROM ${this.tbl}`
	}

	where(kondisi) {
		var kondisi_new = ``
		for (const key in kondisi) {
			kondisi_new+=`${key} = ${this.amankan(kondisi[key])} and `
		}
		this.query += ` WHERE ${kondisi_new.substring(0, kondisi_new.length-4)}`
	}

	limit(new_limit) {
		this.query += ` LIMIT ${new_limit}`
	}

	get_query() {
		return this.query
	}

	run(callback) {
		db.query(this.query, (eror, ress) => {
			this.err = eror
			this.res = ress
			callback(eror, ress)
		})
	}

	// err() {
	// 	return this.err
	// }

	// res() {
	// 	return this.res
	// }

	// mengamankan input
	amankan(str) {
		return db.escape(str)
	}
}

module.exports = Model