const db = require('./db')

// buat tabel user
db.query(`create table user (
		id bigint(15) primary key auto_increment,
		username varchar(255) unique,
		password text,
		name varchar(255) null,
		email varchar(255),
		note text,
		bos bigint(15) null,
		status varchar(255),
		created_at datetime,
		updated_at datetime,
		deleted_at datetime null
	)`, (err, result) => {
	if (err) {throw err}
	console.log('Berhasil membuat tabel user')
})


// buat tabel login
db.query(`create table login (
		id bigint(15) primary key auto_increment,
		token text,
		id_user bigint(15) unique,
		created_at datetime,
		updated_at datetime,
		foreign key (id_user) references user(id)
	)`, (err, result) => {
	if (err) {throw err}
	console.log('Berhasil membuat tabel login')
})